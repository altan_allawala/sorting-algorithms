#!/usr/bin/env python

import math
from scipy import random
from numpy import median
import urllib2
import time

class Algo (object): # Find the number of Split Inversions in a list using the MergeSort algorithm. Written by Altan Allawala.

	def __init__(self):

#		self.input = [[int(i) for i in line.split()][0] for line in open("test.txt")] # Load unsorted array of integers into variable
		self.input = [[int(i) for i in line.split()][0] for line in open("IntegerArray.txt")] # Load unsorted array of integers into variable
		self.SplitInversions = 0


	def Merge(self, A, B): # Merge two lists A and B

		C = []

		n = len(A)
		m = len(B)

		i = 0
		j = 0

		for k in range(m+n):
			while((i!=n) and (j!=m)):
				if(A[i] <= B[j]):
					C.append(A[i])
					i += 1
#					print C, "i"
				else:
					C.append(B[j])
					j += 1
					self.SplitInversions += n-i
#					print C, "j"

		if(i==n):
			C = C + B[j:m]
#			print C, "a"
		if(j==m):
			C = C + A[i:n]
#			print C, "b"

		return C
#		print C


	def Splitter(self, L): # Split an unsorted list into two smaller unsorted lists

		n = len(L)
		m = int(math.floor(n/2)) # Index at which to split the array i.e. start the second array

#		print n, m

		A = L[0:m]
		B = L[m:n]

#		print A
#		print B
		return A, B


	def MergeSort(self, A):

#		print A
		n = len(A)

		if(n==1):
			return A
		else:
			[B,C] = self.Splitter(A)
#			print "B, C", B, C
			B = self.MergeSort(B)
			C = self.MergeSort(C)
			A = self.Merge(B,C)
#			print "A merged", A, "\n"

#		print A, "\n\n"
		return A
		


p = Algo()
p.MergeSort(p.input)
print p.SplitInversions
