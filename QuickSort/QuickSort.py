#!/usr/bin/env python

import math
from scipy import random
from numpy import median
import urllib2
import time

class Algo (object): # Sort an unsorted list of integers using the QuickSort algorithm. Written by Altan Allawala.

	def __init__(self):

		self.input = [[int(i) for i in line.split()][0] for line in open("QuickSort.txt")] # Load unsorted array of integers into variable
#		self.input = [[int(i) for i in line.split()][0] for line in urllib2.urlopen("https://dl.dropboxusercontent.com/u/20888180/AlgI_wk2_testcases/1000.txt")] # Load unsorted array of integers into variable
		self.Comparisons = 0


	def MedianOfThree(self, A, a, c):

		b = int(math.floor((a + c)/2)) # Index of the middle element

		if ( A[b] <= max(A[a],A[c]) and A[b] >= min(A[a],A[c]) ):
			return A[b]

		if ( A[c] <= max(A[a],A[b]) and A[c] >= min(A[a],A[b]) ):
			return A[c]

		if ( A[a] <= max(A[b],A[c]) and A[a] >= min(A[b],A[c]) ):
			return A[a]


	def ChoosePivot(self, A, first, last): # Choose a pivot only between the indices first and last (inclusive)

#		p = A[first] # Choose first element
#		p = A[last] # Choose last element
		p = A[random.randint(first,last)] # Choose randomly
#		p = int(math.floor(median(A[first:last+1]))) # Choose median using Python's median function
#		p = self.MedianOfThree(A, first, last) # Use median-of-three pivot

		return p


	def Swap(self, A, i, j):

		temp2 = A[i]
		A[i] = A[j]
		A[j] = temp2


	def Partition(self, A, p, first, last): # Partition an array only between the indices first and last (inclusive)

		pi = A.index(p) # index of pivot

		self.Swap(A,first,pi) # Ensure pivot is the first entry of the array

		i = first + 1

		for j in range(first+1, last+1):
			if(A[j] < p):
				self.Swap(A,i,j)
				i += 1

		self.Swap(A,first,i-1) # Swap pivot into its rightful place

		self.Comparisons += last - first


	def QuickSort(self, A, i, j):

		if(j>i):
			p = self.ChoosePivot(A,i,j)
			self.Partition(A,p,i,j)
			self.QuickSort(A,i,p-2)
			self.QuickSort(A,p,j)


p = Algo()
p.QuickSort(p.input,0,len(p.input)-1)
print p.Comparisons
