# README #

In Inversion_Counter, InversionCounter.py will count the number of split inversions in a list called IntegerArray.txt. The elements need not be sorted or unique.

In QuickSort, QuickSort.py will sort in ascending order the elements in a list called QuickSort.txt. The elements need not be unique.

Both codes run in big-Oh of N log(N) time.

I wrote these codes while learning about sorting algorithms (with an emphasis on MergeSort and QuickSort).